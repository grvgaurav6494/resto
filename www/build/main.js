webpackJsonp([12],{

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DishlistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data_data__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__veg_veg__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__nonveg_nonveg__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__cart_cart__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__signup_signup__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the DishlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DishlistPage = (function () {
    function DishlistPage(events, storage, navCtrl, navParams, data) {
        var _this = this;
        this.events = events;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = data;
        this.page1 = __WEBPACK_IMPORTED_MODULE_4__veg_veg__["a" /* VegPage */];
        this.page2 = __WEBPACK_IMPORTED_MODULE_5__nonveg_nonveg__["a" /* NonvegPage */];
        this.page3 = __WEBPACK_IMPORTED_MODULE_6__cart_cart__["a" /* CartPage */];
        this.itemcount = 0;
        this.chkbtn = false;
        this.events.subscribe('showCheckout', function (val) {
            _this.chkbtn = val;
        });
    }
    DishlistPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DishlistPage');
    };
    DishlistPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.data.currentMessage.subscribe(function (message) {
            _this.message = message;
        });
    };
    DishlistPage.prototype.slideToIndex = function (index) {
        this.superTabs.slideTo(index);
    };
    DishlistPage.prototype.hideToolbar = function (value) {
        this.superTabs.showToolbar(!value);
    };
    DishlistPage.prototype.item = function (value) {
        if (value == '+') {
            this.itemcount++;
        }
        else {
            if (this.itemcount != 0) {
                this.itemcount--;
            }
        }
    };
    DishlistPage.prototype.onTabSelect = function (val) {
        // this.data.sendtype(val.index); 
    };
    DishlistPage.prototype.currentDish = function ($event) {
    };
    DishlistPage.prototype.getItems = function (val) {
    };
    DishlistPage.prototype.checkout = function () {
        var _this = this;
        this.storage.get('userDetails').then(function (user) {
            if (user == undefined) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__signup_signup__["a" /* SignupPage */]);
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__["a" /* SuperTabs */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__["a" /* SuperTabs */])
    ], DishlistPage.prototype, "superTabs", void 0);
    DishlistPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dishlist',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/dishlist/dishlist.html"*/'<!--\n  Generated template for the DishlistPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{message}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar>\n    <super-tabs (MessageEvent)="currentDish($event)" (tabSelect)="onTabSelect($event)">\n        <super-tab [root]="page1" title="Veg"></super-tab>\n        <super-tab [root]="page2" title="NonVeg"></super-tab>\n        <super-tab [root]="page3" title="Selected"></super-tab>\n    </super-tabs>\n    <button ion-button color="danger" *ngIf="chkbtn" (click)="checkout()" class="checkstyle">Checkout</button>\n</ion-content>\n'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/dishlist/dishlist.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Events */], __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_data_data__["a" /* DataProvider */]])
    ], DishlistPage);
    return DishlistPage;
}());

//# sourceMappingURL=dishlist.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VegPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data_data__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the VegPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VegPage = (function () {
    function VegPage(events, navCtrl, navParams, data) {
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = data;
        this.selected = [];
        this.itemcount = 0;
        this.header = false;
        this.dishlist = [];
        this.typecount = 0;
        this.type = 0;
        this.dish = [];
    }
    VegPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.value = this.navParams.get('value');
        // var selectdish=this.navParams.get('dish');
        if (this.value == 'cart') {
            this.header = true;
        }
        this.dish = [{
                Soups: [
                    {
                        'type': 'veg',
                        'subtype': 'chinese',
                        'id': 'VM1',
                        'name': 'Veg Manchow Soup',
                        'status': 'active',
                        'price': '160',
                        'description': 'Chinese soup india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'veg',
                        'subtype': 'chinese',
                        'id': 'VT1',
                        'name': 'Veg tomato Soup',
                        'status': 'active',
                        'price': '150',
                        'description': 'Tomato soup india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'indian',
                        'id': 'NVC1',
                        'name': 'Chicken Mushroom Soup',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chinese chicken soup india mai zyada chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'chinese',
                        'id': 'NVCM1',
                        'name': 'Nonveg chowmein Soup',
                        'status': 'active',
                        'price': '220',
                        'description': 'Chinese CHOWMEIN soup india mai chalta hai.',
                        'count': 0
                    }
                ],
                Biryani: [
                    {
                        'type': 'veg',
                        'subtype': 'indian',
                        'id': 'VB1',
                        'name': 'Veg Biryani',
                        'status': 'active',
                        'price': '180',
                        'description': 'Biryani india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'indian',
                        'id': 'NVCB1',
                        'name': 'Chicken Biryani',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chicken biryani india mai zyada chalta hai.',
                        'count': 0
                    }
                ],
                Starters: [
                    {
                        'type': 'veg',
                        'subtype': 'chinese',
                        'id': 'SVC1',
                        'name': 'Veg Crispy',
                        'status': 'active',
                        'price': '140',
                        'description': 'Chinese veg crispy india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'chinese',
                        'id': 'NVCC1',
                        'name': 'Chicken crispy',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chinese chicken crispy india mai zyada chalta hai.',
                        'count': 0
                    }
                ],
                Curries: [
                    {
                        'type': 'veg',
                        'subtype': 'indian',
                        'id': 'VAM1',
                        'name': 'Allu mutter',
                        'status': 'active',
                        'price': '160',
                        'description': 'Allu mutter india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'indian',
                        'id': 'NVCM1',
                        'name': 'Chicken Masala fry',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chinese masala fry india mai zyada chalta hai.',
                        'count': 0
                    }
                ],
                Kebabs: [
                    {
                        'type': 'veg',
                        'subtype': 'indian',
                        'id': 'VAM1',
                        'name': 'Mix veg Kebabs',
                        'status': 'active',
                        'price': '160',
                        'description': 'Mix veg kebab india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'indian',
                        'id': 'NVCM1',
                        'name': 'Chicken Masala fry',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chinese masala fry india mai zyada chalta hai.',
                        'count': 0
                    }
                ],
                Chinese: [
                    {
                        'type': 'veg',
                        'subtype': 'chinese',
                        'id': 'VAM1',
                        'name': 'Veg Fried Rice',
                        'status': 'active',
                        'price': '160',
                        'description': 'Veg fried rice india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'indian',
                        'id': 'NVCM1',
                        'name': 'Chicken Fried Rice',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chicken fried rice india mai zyada chalta hai.',
                        'count': 0
                    }
                ]
            }];
        this.data.vegMessage.subscribe(function (prevveg) {
            _this.data.currentMessage.subscribe(function (selectdish) {
                var plength = prevveg.length;
                if (plength != 0) {
                    _this.dish = prevveg;
                    var cuisine = prevveg[0][selectdish];
                    for (var x = 0; x < cuisine.length; x++) {
                        for (var y = 0; y < plength; y++) {
                            if (cuisine[x].name == prevveg[y].name) {
                                cuisine[x] = prevveg[y]; //now cuisine contains previous values
                            }
                        }
                    }
                }
                else {
                    var cuisine = _this.dish[0][selectdish]; //first time visiting the veg page
                }
                _this.dishlist = []; //emptying previous dishlist when code executes from quantity event
                for (var x = 0; x < cuisine.length; x++) {
                    if (cuisine[x].type == 'veg') {
                        _this.dishlist.push(cuisine[x]);
                    }
                }
            });
        });
    };
    VegPage.prototype.ionViewDidLoad = function () {
        var butwidth = $('.quantbut').width();
        $('ion-input').width(butwidth + 32);
        console.log('ionViewDidLoad SubdishlistPage');
    };
    VegPage.prototype.quantity = function (operation, pos, dish) {
        if (operation == '+') {
            var replicaid = false;
            this.dishlist[pos].count++;
            this.events.publish('showCheckout', true);
        }
        else {
            if (this.dishlist[pos].count != 0) {
                this.dishlist[pos].count--;
                if (this.dishlist[pos].count == 0) {
                    var count = 0;
                    for (var x = 0; x < this.dishlist.length; x++) {
                        if (this.dishlist[x].count == 0) {
                            count++;
                        }
                    }
                    if (this.dishlist.length == count) {
                        this.events.publish('showCheckout', false);
                    }
                }
            }
        }
        this.data.vegDish(this.dish); //sending updated veg dishes
    };
    VegPage.prototype.ionViewWillLeave = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__["a" /* SuperTabs */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__["a" /* SuperTabs */])
    ], VegPage.prototype, "superTabs", void 0);
    VegPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-veg',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/veg/veg.html"*/'<!--\n  Generated template for the SubdishlistPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header class="text-center" *ngIf="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{value}}</ion-title>\n    <ion-icon name="cart" class="pull-right iconstyle"></ion-icon>\n    <ion-icon name="contact" class="pull-right iconstyle"></ion-icon>\n   \n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n\n  <ion-card class="margauto">\n    <ion-grid>\n      <ion-row class="margr0" *ngFor="let dish of dishlist;let i=index">\n        <ion-col col-5 class="mid">\n          <img src="assets/imgs/1.jpeg" style="height:90%" />\n        </ion-col>\n        <ion-col col-5>\n          <ion-card-content class="pad0">\n            <ion-card-title>\n              {{dish.name}}\n            </ion-card-title>\n            <p style="font-size:65%">\n              {{dish.description}}\n            </p>\n          </ion-card-content>\n        </ion-col>\n        <ion-col col-2 class="pull-right">\n          <button ion-button class="quantbut" text-center color="dark" outline (click)="quantity(\'+\',i,dish)">\n              <ion-icon name="add"></ion-icon>\n          </button>\n          <ion-input type="text" text-center [value]="dish.count" placeholder="Enter quantity"></ion-input>\n          <button ion-button class="quantbut" color="dark" text-center outline (click)="quantity(\'-\',i,dish)">\n              <ion-icon name="remove"></ion-icon>\n          </button>  \n      </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/veg/veg.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_data_data__["a" /* DataProvider */]])
    ], VegPage);
    return VegPage;
}());

//# sourceMappingURL=veg.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NonvegPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data_data__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the NonvegPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NonvegPage = (function () {
    function NonvegPage(events, navCtrl, navParams, data) {
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = data;
        this.selected = [];
        this.itemcount = 0;
        this.header = false;
        this.dishlist = [];
        this.typecount = 0;
        this.type = 0;
        this.dish = [];
    }
    NonvegPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.value = this.navParams.get('value');
        // var selectdish=this.navParams.get('dish');
        if (this.value == 'cart') {
            this.header = true;
        }
        this.dish = [{
                Soups: [
                    {
                        'type': 'veg',
                        'subtype': 'chinese',
                        'id': 'VM1',
                        'name': 'Veg Manchow Soup',
                        'status': 'active',
                        'price': '160',
                        'description': 'Chinese soup india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'veg',
                        'subtype': 'chinese',
                        'id': 'VT1',
                        'name': 'Veg tomato Soup',
                        'status': 'active',
                        'price': '150',
                        'description': 'Tomato soup india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'indian',
                        'id': 'NVC1',
                        'name': 'Chicken Mushroom Soup',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chinese chicken soup india mai zyada chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'chinese',
                        'id': 'NVCM1',
                        'name': 'Nonveg chowmein Soup',
                        'status': 'active',
                        'price': '220',
                        'description': 'Chinese CHOWMEIN soup india mai chalta hai.',
                        'count': 0
                    }
                ],
                Biryani: [
                    {
                        'type': 'veg',
                        'subtype': 'indian',
                        'id': 'VB1',
                        'name': 'Veg Biryani',
                        'status': 'active',
                        'price': '180',
                        'description': 'Biryani india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'indian',
                        'id': 'NVCB1',
                        'name': 'Chicken Biryani',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chicken biryani india mai zyada chalta hai.',
                        'count': 0
                    }
                ],
                Starters: [
                    {
                        'type': 'veg',
                        'subtype': 'chinese',
                        'id': 'SVC1',
                        'name': 'Veg Crispy',
                        'status': 'active',
                        'price': '140',
                        'description': 'Chinese veg crispy india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'chinese',
                        'id': 'NVCC1',
                        'name': 'Chicken crispy',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chinese chicken crispy india mai zyada chalta hai.',
                        'count': 0
                    }
                ],
                Curries: [
                    {
                        'type': 'veg',
                        'subtype': 'indian',
                        'id': 'VAM1',
                        'name': 'Allu mutter',
                        'status': 'active',
                        'price': '160',
                        'description': 'Allu mutter india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'indian',
                        'id': 'NVCM1',
                        'name': 'Chicken Masala fry',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chinese masala fry india mai zyada chalta hai.',
                        'count': 0
                    }
                ],
                Kebabs: [
                    {
                        'type': 'veg',
                        'subtype': 'indian',
                        'id': 'VAM1',
                        'name': 'Mix veg Kebabs',
                        'status': 'active',
                        'price': '160',
                        'description': 'Mix veg kebab india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'indian',
                        'id': 'NVCM1',
                        'name': 'Chicken Masala fry',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chinese masala fry india mai zyada chalta hai.',
                        'count': 0
                    }
                ],
                Chinese: [
                    {
                        'type': 'veg',
                        'subtype': 'chinese',
                        'id': 'VAM1',
                        'name': 'Veg Fried Rice',
                        'status': 'active',
                        'price': '160',
                        'description': 'Veg fried rice india mai chalta hai.',
                        'count': 0
                    },
                    {
                        'type': 'nonveg',
                        'subtype': 'indian',
                        'id': 'NVCM1',
                        'name': 'Chicken Fried Rice',
                        'status': 'active',
                        'price': '200',
                        'description': 'Chicken fried rice india mai zyada chalta hai.',
                        'count': 0
                    }
                ]
            }];
        this.data.vegMessage.subscribe(function (prevnonveg) {
            _this.data.currentMessage.subscribe(function (selectdish) {
                var plength = prevnonveg.length;
                if (plength != 0) {
                    _this.dish = prevnonveg;
                    var cuisine = prevnonveg[0][selectdish];
                    for (var x = 0; x < cuisine.length; x++) {
                        for (var y = 0; y < plength; y++) {
                            if (cuisine[x].name == prevnonveg[y].name) {
                                cuisine[x] = prevnonveg[y]; //get previous values
                            }
                        }
                    }
                }
                else {
                    var cuisine = _this.dish[0][selectdish];
                }
                _this.dishlist = []; //emptying previous dishlist when code executes from quantity event
                for (var x = 0; x < cuisine.length; x++) {
                    if (cuisine[x].type == 'nonveg') {
                        _this.dishlist.push(cuisine[x]);
                    }
                }
            });
        });
    };
    NonvegPage.prototype.ionViewDidLoad = function () {
        var butwidth = $('.quantbut').width();
        $('ion-input').width(butwidth + 32);
        console.log('ionViewDidLoad SubdishlistPage');
    };
    NonvegPage.prototype.quantity = function (operation, pos, dish) {
        if (operation == '+') {
            var replicaid = false;
            this.dishlist[pos].count++;
            this.events.publish('showCheckout', true);
        }
        else {
            if (this.dishlist[pos].count != 0) {
                this.dishlist[pos].count--;
                if (this.dishlist[pos].count == 0) {
                    var count = 0;
                    for (var x = 0; x < this.dishlist.length; x++) {
                        if (this.dishlist[x].count == 0) {
                            count++;
                        }
                    }
                    if (this.dishlist.length == count) {
                        this.events.publish('showCheckout', false);
                    }
                }
            }
        }
        this.data.vegDish(this.dish); //sending updated nonveg dishes
    };
    NonvegPage.prototype.ionViewWillLeave = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__["a" /* SuperTabs */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__["a" /* SuperTabs */])
    ], NonvegPage.prototype, "superTabs", void 0);
    NonvegPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-nonveg',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/nonveg/nonveg.html"*/'<!--\n  Generated template for the SubdishlistPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header class="text-center" *ngIf="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{value}}</ion-title>\n    <ion-icon name="cart" class="pull-right iconstyle"></ion-icon>\n    <ion-icon name="contact" class="pull-right iconstyle"></ion-icon>\n   \n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n\n  <ion-card class="margauto">\n    <ion-grid>\n      <ion-row class="margr0" *ngFor="let dish of dishlist;let i=index">\n        <ion-col col-5 class="mid">\n          <img src="assets/imgs/1.jpeg" style="height:90%" />\n        </ion-col>\n        <ion-col col-5>\n          <ion-card-content class="pad0">\n            <ion-card-title>\n              {{dish.name}}\n            </ion-card-title>\n            <p style="font-size:65%">\n              {{dish.description}}\n            </p>\n          </ion-card-content>\n        </ion-col>\n        <ion-col col-2 class="pull-right">\n            <button ion-button class="quantbut" text-center color="dark" outline (click)="quantity(\'+\',i,dish)">\n                <ion-icon name="add"></ion-icon>\n            </button>\n            <ion-input type="text" text-center [value]="dish.count" placeholder="Enter quantity"></ion-input>\n            <button ion-button class="quantbut" color="dark" text-center outline (click)="quantity(\'-\',i,dish)">\n                <ion-icon name="remove"></ion-icon>\n            </button>  \n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/nonveg/nonveg.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_data_data__["a" /* DataProvider */]])
    ], NonvegPage);
    return NonvegPage;
}());

//# sourceMappingURL=nonveg.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SignupPage = (function () {
    function SignupPage(navCtrl, navParams, fb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        var pattern = '/^(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/|^[789]\d{9}$)$/';
        var mobile = "^[789]\d{9}$)$/";
        this.signupForm = this.fb.group({
            mobile: __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].pattern(pattern), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].minLength(10), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(10)]),
            name: '',
            password: '',
            repassword: ''
        });
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.signup = function () {
        debugger;
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/signup/signup.html"*/'<!--\n  Generated template for the SignupPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>signup</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n\n<ion-content padding class="bg">\n  <ion-grid class="titlepos">\n    <ion-row>\n      <ion-col class="checkpos" col-12>\n          <h1>Dummy</h1>\n          <h4><span>SignUp</span></h4>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12>\n          <ion-list>\n            <form [formGroup]="signupForm" (ngSubmit)="signup()">\n              <ion-item>\n                  <ion-label floating class="labelcolor">Enter Your Mobile Number</ion-label>\n                  <ion-input type="text" class="signcolor" formControlName="mobile"></ion-input>\n                  <span class="help-block required" *ngIf="signupForm.controls.mobile.invalid && (signupForm.controls.mobile.touched || !signupForm.controls.mobile.pristine)">Error</span>\n              </ion-item>\n            <div id="signup" *ngIf="false"> \n            <ion-item>\n                <ion-label floating class="labelcolor">Enter Your Name</ion-label>\n                <ion-input type="text" class="signcolor" formControlName="name"></ion-input>\n            </ion-item>\n            \n            <ion-item>\n                <ion-label floating class="labelcolor">Enter <span>Your Choice </span>Password</ion-label>\n                <ion-input type="password" class="signcolor" formControlName="password"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label floating class="labelcolor">Re-enter Password</ion-label>\n                <ion-input type="password" class="signcolor" formControlName="repassword"></ion-input>\n            </ion-item>\n            </div>\n            <button type="submit" ion-button col-12 class="subcolor" [disabled]="!signupForm.valid">Signup</button>\n            </form>\n          </ion-list>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col col-12>\n          <h4 class="subtitlesign"><span>OR</span></h4>\n          <button ion-button block color="danger"><ion-icon class="signicons" name="logo-google"></ion-icon>Login in with Gmail</button>\n          <button ion-button block><ion-icon class="signicons" name="logo-facebook"></ion-icon>Login in with Facebook</button>\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/signup/signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_menu__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FirstPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FirstPage = (function () {
    function FirstPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FirstPage.prototype.ngOnInit = function () {
    };
    FirstPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FirstPage');
    };
    FirstPage.prototype.next = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__menu_menu__["a" /* MenuPage */]);
    };
    FirstPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-first',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/first/first.html"*/'<!--\n  Generated template for the FirstPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>first</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n\n\n<ion-content class="background">\n  <section class="layer">\n    <h1 class="Title">Dummy</h1>\n        <h4 class="subtitle"><span>Bar & Resto</span></h4>\n  </section>\n</ion-content>\n<ion-footer class="pad0">\n  <ion-toolbar positon="bottom" class="pad0">\n    <div class="col-md-12 col-xs-12 pad0">\n      <button ion-button color="light" full (click)="next()">Check our Menu</button>\n    </div>\n    <!-- <div class="col-md-6 col-xs-6 pad0">\n      <button ion-button  color="light" full (click)="next(\'signup\')">Signup</button>\n    </div> -->\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/first/first.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], FirstPage);
    return FirstPage;
}());

//# sourceMappingURL=first.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dishlist_dishlist__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data_data__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cart_cart__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MenuPage = (function () {
    function MenuPage(navCtrl, navParams, data) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = data;
    }
    MenuPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MenuPage');
    };
    MenuPage.prototype.ngOnInit = function () {
    };
    MenuPage.prototype.ngAfterViewInit = function () {
        this.slides.autoplay = 1500;
        this.slides.loop = true;
    };
    MenuPage.prototype.gotoSlide = function () {
        this.slides.startAutoplay();
    };
    MenuPage.prototype.slideChanged = function () {
        // let currentIndex=this.slides.slideNext(500);
        this.slides.startAutoplay();
    };
    MenuPage.prototype.godish = function (value) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__dishlist_dishlist__["a" /* DishlistPage */]);
        this.data.sendmsg(value);
    };
    MenuPage.prototype.goCart = function () {
        var val = {
            'value': 'cart'
        };
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__cart_cart__["a" /* CartPage */], val);
    };
    MenuPage.prototype.goProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__profile_profile__["a" /* ProfilePage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Slides */])
    ], MenuPage.prototype, "slides", void 0);
    MenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/menu/menu.html"*/'<!--\n  Generated template for the MenuPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header class="text-center">\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>Menu</ion-title>\n      <ion-icon name="cart" (click)="goCart()" class="pull-right iconstyle"></ion-icon>\n      <ion-icon name="contact" (click)="goProfile()" class="pull-right iconstyle"></ion-icon>\n     \n    </ion-navbar>\n  </ion-header>\n\n<ion-content>\n    <ion-slides class="size" (ionSlideDidChange)="slideChanged()">\n        <ion-slide>\n          <img src="assets/imgs/1.jpg">\n        </ion-slide>\n        <ion-slide>\n            <img src="assets/imgs/2.jpg">\n        </ion-slide>\n        <ion-slide>\n            <img src="assets/imgs/3.jpg">\n        </ion-slide>\n    </ion-slides>\n    <!-- <button ion-button color="light" full (click)="gotoSlide()">Next</button> -->\n\n    <ion-grid class="image">\n      <ion-row>\n        <ion-col col-6 class="text-center bg1">\n          <div class="blacklayer" (click)="godish(\'Soups\')">\n              <h3 class="text-white pull-left textpos" col-11>Soups\n              <i class="fas fa-angle-right text-white pull-right"></i></h3>\n          </div>\n        </ion-col>\n        <ion-col col-6 class="text-center bg2">\n          <div class="blacklayer" (click)="godish(\'Biryani\')">\n              <h3 class="text-white pull-left textpos" col-11>Biryani\n              <i class="fas fa-angle-right text-white pull-right"></i></h3>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 class="text-center bg3">\n          <div class="blacklayer" (click)="godish(\'Starters\')">\n              <h3 class="text-white pull-left textpos" col-11>Starters\n              <i class="fas fa-angle-right text-white pull-right"></i></h3>\n          </div>\n        </ion-col>\n        <ion-col col-6 class="text-center bg4">\n          <div class="blacklayer" (click)="godish(\'Kebabs\')">\n              <h3 class="text-white pull-left textpos" col-11>Kebabs\n              <i class="fas fa-angle-right text-white pull-right"></i></h3>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 class="text-center bg5">\n          <div class="blacklayer" (click)="godish(\'Curries\')">\n              <h3 class="text-white pull-left textpos" col-11>Curries\n              <i class="fas fa-angle-right text-white pull-right"></i></h3>\n          </div>\n        </ion-col>\n        <ion-col col-6 class="text-center bg6">\n          <div class="blacklayer" (click)="godish(\'Chinese\')">\n              <h3 class="text-white pull-left textpos" col-11>Chinese\n              <i class="fas fa-angle-right text-white pull-right"></i></h3>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n   \n</ion-content>\n'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/menu/menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_data_data__["a" /* DataProvider */]])
    ], MenuPage);
    return MenuPage;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/profile/profile.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-header>\n      Name\n      <button ion-button color="danger" class="pull-right editstyle">Edit</button>\n    </ion-card-header>\n    <ion-card-content>\n      Gaurav\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      Mobile\n      <button ion-button color="danger" class="pull-right editstyle">Edit</button>\n    </ion-card-header>\n    <ion-card-content>\n      7506936046\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      Password\n      <button ion-button color="danger" class="pull-right editstyle">Edit</button>\n    </ion-card-header>\n    <ion-card-content>\n      ********\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      Address\n      <button ion-button color="danger" class="pull-right editstyle">Edit</button>\n    </ion-card-header>\n    <ion-card-content>\n      <div>\n        <label>Home</label>\n        <p>addresss homeeee</p>\n      </div>\n      <div class="topspace">\n        <label>Home</label>\n        <p>addresss homeeee</p>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 125:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 125;

/***/ }),

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/billsummary/billsummary.module": [
		308,
		11
	],
	"../pages/cart/cart.module": [
		309,
		10
	],
	"../pages/dishlist/dishlist.module": [
		310,
		9
	],
	"../pages/first/first.module": [
		311,
		8
	],
	"../pages/login/login.module": [
		312,
		7
	],
	"../pages/menu/menu.module": [
		313,
		6
	],
	"../pages/nonveg/nonveg.module": [
		314,
		5
	],
	"../pages/payment/payment.module": [
		315,
		4
	],
	"../pages/paymenttype/paymenttype.module": [
		316,
		3
	],
	"../pages/profile/profile.module": [
		317,
		2
	],
	"../pages/signup/signup.module": [
		318,
		1
	],
	"../pages/veg/veg.module": [
		319,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 166;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <h3>Ionic Menu Starter</h3>\n\n  <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will show you the way.\n  </p>\n\n  <button ion-button secondary menuToggle>Toggle Menu</button>\n</ion-content>\n'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/list/list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BillsummaryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the BillsummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BillsummaryPage = (function () {
    function BillsummaryPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
    }
    BillsummaryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BillsummaryPage');
    };
    BillsummaryPage.prototype.getAddress = function () {
        this.listAddress();
    };
    BillsummaryPage.prototype.listAddress = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Enter Address',
            cssClass: 'radioname',
            inputs: [
                {
                    type: 'radio',
                    label: 'Address1',
                    value: 'address1 value'
                },
                {
                    type: 'radio',
                    label: 'Address2',
                    value: 'address value2'
                },
                {
                    type: 'radio',
                    label: 'Address3',
                    value: 'address3 value'
                },
                {
                    type: 'radio',
                    label: 'Address4',
                    value: 'address 4 value',
                },
                {
                    type: 'radio',
                    label: 'Address5',
                    value: 'address value 5'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Add Address',
                    handler: function (data) {
                        _this.newAddress();
                    }
                },
                {
                    text: 'Confirm Order',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    BillsummaryPage.prototype.newAddress = function () {
        var alert = this.alertCtrl.create({
            title: 'Enter Address',
            inputs: [
                {
                    name: 'Pincode',
                    placeholder: 'Enter Pincode'
                },
                {
                    name: 'First Line',
                    placeholder: 'First Line Address'
                },
                {
                    name: 'Second line',
                    placeholder: 'Second line Address'
                },
                {
                    name: 'City',
                    placeholder: 'Enter City'
                },
                {
                    name: 'State',
                    placeholder: 'Enter State'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Add Address',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    BillsummaryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-billsummary',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/billsummary/billsummary.html"*/'<!--\n  Generated template for the BillsummaryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>billsummary</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-grid>\n        <ion-row>\n          <ion-col col-12 class="center">\n            <h3 class="name">Gaurav Shetty</h3>\n          </ion-col>\n          <ion-col col-12 class="center">\n            <h6 class="name">Order id 415asd23as</h6>\n          </ion-col>\n        </ion-row>\n    </ion-grid>\n\n    <ion-grid class="itempad">\n      <ion-row>\n        <ion-col col-12 class="botstyle">\n          Your Order\n        </ion-col>\n        <ion-col col-12>\n          <ion-col col-8 no-padding>\n            1&nbsp;x&nbsp;Lasagne shroom\n          </ion-col>\n          <ion-col col-4>\n            <span class="pull-right"><i class="fas fa-rupee-sign"></i>&nbsp;180</span>\n          </ion-col>\n        </ion-col>\n        <ion-col col-12>\n            <ion-col col-8 no-padding>\n              2&nbsp;x&nbsp;Santino Burger\n            </ion-col>\n            <ion-col col-4>\n              <span class="pull-right"><i class="fas fa-rupee-sign"></i>&nbsp;380</span>\n            </ion-col>\n        </ion-col>\n       \n      </ion-row>\n    </ion-grid>\n\n    <ion-grid class="itempad">\n      <ion-row>\n        <ion-col col-12 class="text-center">\n          Discount Code(Optional)\n        </ion-col>\n        <ion-col col-12 class="eeebg">\n          <!-- <ion-list class="margbot"> -->\n          <ion-item>\n            <ion-label class="codecolor">CODE</ion-label>\n            <ion-input type="text"></ion-input>\n          </ion-item>\n          <!-- </ion-list> -->\n        </ion-col>\n        <ion-col col-12>\n          <p class="font10">40% discount applied</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid class="itempad">\n        <ion-row>\n          <!-- <ion-col col-12 class="botstyle">\n            Your Order\n          </ion-col> -->\n          <ion-col col-12>\n            <ion-col col-8 no-padding>\n              Total without discount\n            </ion-col>\n            <ion-col col-4>\n              <span class="pull-right"><i class="fas fa-rupee-sign"></i>&nbsp;590</span>\n            </ion-col>\n          </ion-col>\n          <ion-col col-12>\n              <ion-col col-8 no-padding>\n                GST(5%)\n              </ion-col>\n              <ion-col col-4>\n                <span class="pull-right"><i class="fas fa-rupee-sign"></i>&nbsp;24</span>\n              </ion-col>\n          </ion-col>\n          <ion-col col-12>\n              <ion-col col-8 no-padding class="red">\n                Discount(MACK50)\n              </ion-col>\n              <ion-col col-4 class="red">\n                <span class="pull-right"><i class="fas fa-rupee-sign"></i>&nbsp;100</span>\n              </ion-col>\n          </ion-col>\n         \n        </ion-row>\n      </ion-grid>\n\n    <ion-grid class="totalpad">\n      <ion-row>\n        <ion-col col-4>\n          Total Order\n        </ion-col>\n        <ion-col col-8> \n          <span class="pull-right discprice"><i class="fas fa-rupee-sign"></i>&nbsp;514</span>\n          <del class="pull-right delpos"><i class="fas fa-rupee-sign"></i>590</del>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n          <button col-12 ion-button round color="secondary" (click)="getAddress()">Order</button>\n      </ion-row>\n    </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/billsummary/billsummary.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], BillsummaryPage);
    return BillsummaryPage;
}());

//# sourceMappingURL=billsummary.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Login</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymenttypePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_payment__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PaymenttypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PaymenttypePage = (function () {
    function PaymenttypePage(navCtrl, navParams, renderer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.renderer = renderer;
        this.upiState = false;
        this.cardState = false;
        this.netbankState = false;
        this.codState = false;
        this.paylaterState = false;
        this.upi = [];
        this.paylater = [];
        this.card = [];
        this.netbank = [];
        this.cash = [];
        this.upi = [{ 'type': 'PhonePe/Bhim UPI' }];
        this.paylater = [{ 'type': 'LazyPay' }, { 'type': 'ePayLater' }];
        this.card = [{ 'type': 'Debit Card' }, { 'type': 'Credit Card' }];
        this.netbank = [{ 'type': 'State Bank of India(SBI)' }, { 'type': 'HDFC Bank' }, { 'type': 'ICICI Bank' }, { 'type': 'Canara Bank' }, { 'type': 'Axis Bank' }, { 'type': 'Other Bank' }];
        this.cash = [{ 'type': 'Cash on delivery' }, { 'type': 'Pay Online on delivery' }];
    }
    PaymenttypePage.prototype.ngOnInit = function () {
        this.renderer.setElementStyle(this.upiList.nativeElement, 'webkitTransition', 'max-height 700ms');
        this.renderer.setElementStyle(this.codList.nativeElement, 'webkitTransition', 'max-height 700ms');
        this.renderer.setElementStyle(this.paylaterList.nativeElement, 'webkitTransition', 'max-height 700ms');
        this.renderer.setElementStyle(this.netbankList.nativeElement, 'webkitTransition', 'max-height 700ms');
        this.renderer.setElementStyle(this.cardList.nativeElement, 'webkitTransition', 'max-height 700ms');
    };
    PaymenttypePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PaymentPage');
    };
    PaymenttypePage.prototype.cardSelected = function (value) {
        if (value = "Debit Card") {
        }
    };
    PaymenttypePage.prototype.paymentSelect = function (type) {
        debugger;
        var val = {
            'value': type
        };
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__payment_payment__["a" /* PaymentPage */], val);
    };
    PaymenttypePage.prototype.toggleAcc = function (val) {
        debugger;
        switch (val) {
            case 'upi':
                if (this.upiState) {
                    this.renderer.setElementStyle(this.upiList.nativeElement, 'max-height', '0px');
                }
                else {
                    this.renderer.setElementStyle(this.upiList.nativeElement, 'max-height', '500px');
                    this.renderer.setElementStyle(this.cardList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.netbankList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.codList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.paylaterList.nativeElement, 'max-height', '0px');
                }
                this.upiState = !this.upiState;
                break;
            case 'card':
                if (this.cardState) {
                    this.renderer.setElementStyle(this.cardList.nativeElement, 'max-height', '0px');
                }
                else {
                    this.renderer.setElementStyle(this.cardList.nativeElement, 'max-height', '500px');
                    this.renderer.setElementStyle(this.upiList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.netbankList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.codList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.paylaterList.nativeElement, 'max-height', '0px');
                }
                this.cardState = !this.cardState;
                break;
            case 'netbank':
                if (this.netbankState) {
                    this.renderer.setElementStyle(this.netbankList.nativeElement, 'max-height', '0px');
                }
                else {
                    this.renderer.setElementStyle(this.netbankList.nativeElement, 'max-height', '500px');
                    this.renderer.setElementStyle(this.cardList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.upiList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.codList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.paylaterList.nativeElement, 'max-height', '0px');
                }
                this.netbankState = !this.netbankState;
                break;
            case 'cod':
                if (this.codState) {
                    this.renderer.setElementStyle(this.codList.nativeElement, 'max-height', '0px');
                }
                else {
                    this.renderer.setElementStyle(this.codList.nativeElement, 'max-height', '500px');
                    this.renderer.setElementStyle(this.cardList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.netbankList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.upiList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.paylaterList.nativeElement, 'max-height', '0px');
                }
                this.codState = !this.codState;
                break;
            case 'paylater':
                if (this.paylaterState) {
                    this.renderer.setElementStyle(this.paylaterList.nativeElement, 'max-height', '0px');
                }
                else {
                    this.renderer.setElementStyle(this.paylaterList.nativeElement, 'max-height', '500px');
                    this.renderer.setElementStyle(this.cardList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.netbankList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.codList.nativeElement, 'max-height', '0px');
                    this.renderer.setElementStyle(this.upiList.nativeElement, 'max-height', '0px');
                }
                this.paylaterState = !this.paylaterState;
                break;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('upitype'),
        __metadata("design:type", Object)
    ], PaymenttypePage.prototype, "upiList", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('cardtype'),
        __metadata("design:type", Object)
    ], PaymenttypePage.prototype, "cardList", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('netbanktype'),
        __metadata("design:type", Object)
    ], PaymenttypePage.prototype, "netbankList", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('codtype'),
        __metadata("design:type", Object)
    ], PaymenttypePage.prototype, "codList", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('paylatertype'),
        __metadata("design:type", Object)
    ], PaymenttypePage.prototype, "paylaterList", void 0);
    PaymenttypePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-paymenttype',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/paymenttype/paymenttype.html"*/'<!--\n  Generated template for the PaymentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n      <ion-title>Payment Method</ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n  \n  \n  <ion-content padding>\n  \n        <ion-card>\n            <ion-card-header color=\'dark\' (click)="toggleAcc(\'upi\')">\n              UPI\n            </ion-card-header>\n            <ion-card-content #upitype>\n              <button ion-item *ngFor="let upi of upi" color=\'mycolor\' (click)="paymentSelect(upi.type)">{{upi.type}}</button>\n            </ion-card-content>\n        </ion-card>\n  \n        <ion-card>\n            <ion-card-header color="dark" (click)="toggleAcc(\'card\')">\n              Debit/Credit Card\n            </ion-card-header>\n            <ion-card-content #cardtype>\n              <button ion-item *ngFor="let upi of card" color=\'mycolor\' (click)="paymentSelect(upi.type)">{{upi.type}}</button>\n            </ion-card-content>\n        </ion-card>\n  \n        <ion-card>\n            <ion-card-header color="dark" (click)="toggleAcc(\'netbank\')">\n              Netbanking\n            </ion-card-header>\n            <ion-card-content #netbanktype>\n              <button ion-item *ngFor="let upi of netbank" color=\'mycolor\' (click)="paymentSelect(upi.type)">{{upi.type}}</button>\n            </ion-card-content>\n        </ion-card>\n  \n        <ion-card>\n            <ion-card-header color="dark" (click)="toggleAcc(\'cod\')">\n              Cash on Delivery(COD)\n            </ion-card-header>\n            <ion-card-content #codtype>\n              <button ion-item *ngFor="let upi of cash" color=\'mycolor\' (click)="paymentSelect(upi.type)">{{upi.type}}</button>\n            </ion-card-content>\n        </ion-card>\n  \n        <ion-card>\n            <ion-card-header color="dark" (click)="toggleAcc(\'paylater\')">\n              Pay Later\n            </ion-card-header>\n            <ion-card-content #paylatertype>\n              <button ion-item *ngFor="let upi of paylater" color=\'mycolor\' (click)="paymentSelect(upi.type)">{{upi.type}}</button>\n            </ion-card-content>\n        </ion-card>\n  </ion-content>\n  '/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/paymenttype/paymenttype.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */]])
    ], PaymenttypePage);
    return PaymenttypePage;
}());

//# sourceMappingURL=paymenttype.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(244);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_list_list__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_first_first__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_signup_signup__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_menu_menu__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_dishlist_dishlist__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_veg_veg__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_nonveg_nonveg__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_cart_cart__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_billsummary_billsummary__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_paymenttype_paymenttype__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_payment_payment__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_status_bar__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_splash_screen__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_data_data__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_ionic2_super_tabs__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_storage__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_dishlist_dishlist__["a" /* DishlistPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_veg_veg__["a" /* VegPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_nonveg_nonveg__["a" /* NonvegPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_cart_cart__["a" /* CartPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_billsummary_billsummary__["a" /* BillsummaryPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_paymenttype_paymenttype__["a" /* PaymenttypePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_payment_payment__["a" /* PaymentPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/billsummary/billsummary.module#BillsummaryPageModule', name: 'BillsummaryPage', segment: 'billsummary', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cart/cart.module#CartPageModule', name: 'CartPage', segment: 'cart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dishlist/dishlist.module#DishlistPageModule', name: 'DishlistPage', segment: 'dishlist', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/first/first.module#FirstPageModule', name: 'FirstPage', segment: 'first', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nonveg/nonveg.module#NonvegPageModule', name: 'NonvegPage', segment: 'nonveg', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment/payment.module#PaymentPageModule', name: 'PaymentPage', segment: 'payment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/paymenttype/paymenttype.module#PaymenttypePageModule', name: 'PaymenttypePage', segment: 'paymenttype', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/veg/veg.module#VegPageModule', name: 'VegPage', segment: 'veg', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["e" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_22_ionic2_super_tabs__["b" /* SuperTabsModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_23__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                    name: '__mydb',
                    driverOrder: ['indexeddb', 'sqlite', 'websql']
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_dishlist_dishlist__["a" /* DishlistPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_veg_veg__["a" /* VegPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_nonveg_nonveg__["a" /* NonvegPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_cart_cart__["a" /* CartPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_billsummary_billsummary__["a" /* BillsummaryPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_paymenttype_paymenttype__["a" /* PaymenttypePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_payment_payment__["a" /* PaymentPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_21__providers_data_data__["a" /* DataProvider */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_first_first__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_payment_payment__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_payment_payment__["a" /* PaymentPage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'List', component: __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */] },
            { title: 'first', component: __WEBPACK_IMPORTED_MODULE_6__pages_first_first__["a" /* FirstPage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item class="btncolor" *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav id="nav" [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { HttpClient } from '@angular/common/http';


/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var DataProvider = (function () {
    function DataProvider() {
        this.messageSource = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]('default message');
        this.currentMessage = this.messageSource.asObservable(); //latest value gets stored
        this.vegSource = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        this.vegMessage = this.vegSource.asObservable();
        console.log('Hello DataProvider Provider');
    }
    DataProvider.prototype.sendmsg = function (message) {
        this.messageSource.next(message);
    };
    DataProvider.prototype.vegDish = function (dish) {
        this.vegSource.next(dish);
    };
    DataProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], DataProvider);
    return DataProvider;
}());

//# sourceMappingURL=data.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data_data__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CartPage = (function () {
    function CartPage(events, alertCtrl, storage, navCtrl, navParams, data) {
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = data;
        this.selected = [];
        this.dishpresent = true;
        this.itemcount = 0;
        this.header = false;
        this.dishlist = [];
        this.typecount = 0;
        this.type = 0;
        this.totaldish = [];
    }
    CartPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.navParams.get('value') == 'cart') {
            this.header = true;
            this.value = 'Cart';
        }
        this.dishpresent = true;
        this.data.vegMessage.subscribe(function (vegdish) {
            _this.data.currentMessage.subscribe(function (cuisine) {
                if (vegdish != undefined && vegdish.length != 0) {
                    _this.totaldish = vegdish;
                    var tot = _this.totaldish[0];
                    var dishname = Object.keys(tot);
                    _this.dishlist = []; //for again coming back to cart or adding quantity
                    for (var w = 0; w < dishname.length; w++) {
                        var cuisinedishes = tot[dishname[w]]; //per dish length
                        for (var x = 0; x < cuisinedishes.length; x++) {
                            if (cuisinedishes[x].count > 0) {
                                _this.dishlist.push(cuisinedishes[x]);
                            }
                        }
                    }
                }
                else {
                    _this.dishpresent = false;
                }
            });
        });
    };
    CartPage.prototype.ionViewDidLoad = function () {
        console.log(this.dishlist);
        var butwidth = $('.quantbut').width();
        $('ion-input').width(butwidth + 32);
        console.log('ionViewDidLoad SubdishlistPage');
    };
    CartPage.prototype.quantity = function (operation, pos, dish) {
        if (operation == '+') {
            this.dishlist[pos].count++;
            this.events.publish('showCheckout', true);
        }
        else {
            if (this.dishlist[pos].count != 0) {
                this.dishlist[pos].count--;
                if (this.dishlist[pos].count == 0) {
                    this.showConfirm(dish, pos);
                }
            }
        }
    };
    CartPage.prototype.ionViewWillLeave = function () {
    };
    CartPage.prototype.showConfirm = function (dish, pos) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Remove Item',
            message: 'Are you sure you want to remove yummy ' + dish.name + ' ?',
            buttons: [
                {
                    text: 'Disagree',
                    handler: function () {
                        _this.dishlist[pos].count++;
                    }
                },
                {
                    text: 'Agree',
                    handler: function () {
                        _this.dishlist.pop(dish);
                        if (_this.dishlist.length == 0) {
                            _this.dishpresent = false;
                        }
                        _this.events.publish('showCheckout', false);
                        _this.data.vegDish(_this.totaldish);
                    }
                }
            ]
        });
        confirm.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__["a" /* SuperTabs */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic2_super_tabs__["a" /* SuperTabs */])
    ], CartPage.prototype, "superTabs", void 0);
    CartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cart',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/cart/cart.html"*/'<!--\n  Generated template for the SubdishlistPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header class="text-center" [hidden]="!header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{value}}</ion-title>\n    <ion-icon name="cart" class="pull-right iconstyle"></ion-icon>\n    <ion-icon name="contact" class="pull-right iconstyle"></ion-icon>\n   \n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n\n  <ion-card class="margauto" *ngIf="dishpresent">\n    <ion-grid>\n      <ion-row class="margr0" *ngFor="let dish of dishlist;let i=index">\n        <ion-col col-5 class="mid">\n          <img src="assets/imgs/1.jpeg" style="height:90%" />\n        </ion-col>\n        <ion-col col-5>\n          <ion-card-content class="pad0">\n            <ion-card-title>\n              {{dish.name}}\n            </ion-card-title>\n            <p style="font-size:65%">\n              {{dish.description}}\n            </p>\n          </ion-card-content>\n        </ion-col>\n        <ion-col col-2 class="pull-right">\n          <button ion-button class="quantbut" text-center color="dark" outline (click)="quantity(\'+\',i,dish)">\n              <ion-icon name="add"></ion-icon>\n          </button>\n          <ion-input type="text" text-center [value]="dish.count" placeholder="Enter quantity"></ion-input>\n          <button ion-button class="quantbut" color="dark" text-center outline (click)="quantity(\'-\',i,dish)">\n              <ion-icon name="remove"></ion-icon>\n          </button>  \n      </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n  <ion-card class="margauto" *ngIf="!dishpresent">\n    <ion-card-content>\n      Please select yummy dishes! :)\n    </ion-card-content>\n  \n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/cart/cart.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_data_data__["a" /* DataProvider */]])
    ], CartPage);
    return CartPage;
}());

//# sourceMappingURL=cart.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PaymentPage = (function () {
    function PaymentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.upi = [];
        this.paylater = [];
        this.card = [];
        this.netbank = [];
        this.cash = [];
        this.upi = [{ 'type': 'PhonePe/Bhim UPI' }];
        this.paylater = [{ 'type': 'LazyPay' }, { 'type': 'ePayLater' }];
        this.card = [{ 'type': 'Debit Card' }, { 'type': 'Credit Card' }];
        this.netbank = [{ 'type': 'State Bank of India(SBI)' }, { 'type': 'HDFC Bank' }, { 'type': 'ICICI Bank' }, { 'type': 'Canara Bank' }, { 'type': 'Axis Bank' }, { 'type': 'Other Bank' }];
        this.cash = [{ 'type': 'Cash on delivery' }, { 'type': 'Pay Online on delivery' }];
    }
    PaymentPage.prototype.ionViewWillEnter = function () {
        debugger;
        this.header = this.navParams.get('value');
    };
    PaymentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PaymentPage');
    };
    PaymentPage.prototype.cardSelected = function (value) {
        if (value = "Debit Card") {
        }
    };
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-payment',template:/*ion-inline-start:"/home/webwerks/Desktop/rest/rest/src/pages/payment/payment.html"*/'<!--\n  Generated template for the PaymentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{header}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid padding>\n    <ion-row style="background:#000;border-radius:11px">\n        <!-- <button ion-button color="light" class="changestyle">Change Color</button> -->\n        <article>\n        <section class="first">\n      <ion-col col-9>\n          <ion-item>\n              <ion-label floating color="light" class="text-left padtop0">Enter Card Number</ion-label>\n              <ion-input type="text"></ion-input>\n          </ion-item>\n      </ion-col>\n      <ion-col col-3>\n          <img src="../assets/imgs/cards/visa.svg">\n      </ion-col>\n      <ion-col col-9>\n          <ion-item>\n              <ion-label floating color="light" class="text-left padtop0">Enter expiry date(MM/YY)</ion-label>\n              <ion-input type="text" color="light"></ion-input>\n          </ion-item>\n      </ion-col>\n      <ion-col col-9>\n        <ion-item>\n          <ion-label floating color="light" class="text-left padtop0">Enter your name</ion-label>\n          <ion-input type="text" color="light"></ion-input>\n        </ion-item>\n      </ion-col>\n      </section>\n\n      <section class="first second">\n        \n      </section>\n    </article>\n    </ion-row>\n  </ion-grid> \n  <button ion-button color="mycolor" col-12>Next</button>\n</ion-content>\n'/*ion-inline-end:"/home/webwerks/Desktop/rest/rest/src/pages/payment/payment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], PaymentPage);
    return PaymentPage;
}());

//# sourceMappingURL=payment.js.map

/***/ })

},[222]);
//# sourceMappingURL=main.js.map