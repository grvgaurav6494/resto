// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {
  private messageSource=new BehaviorSubject('default message');
  currentMessage=this.messageSource.asObservable();//latest value gets stored

  private vegSource=new BehaviorSubject([]);
  vegMessage=this.vegSource.asObservable();

  constructor() {
    console.log('Hello DataProvider Provider');
  }

  sendmsg(message){
    this.messageSource.next(message);
  }

  vegDish(dish){
    this.vegSource.next(dish);
  }

}
