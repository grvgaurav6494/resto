import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {FirstPage} from '../pages/first/first';
import {LoginPage} from '../pages/login/login';
import {SignupPage} from '../pages/signup/signup';
import {MenuPage} from '../pages/menu/menu';
import {DishlistPage} from '../pages/dishlist/dishlist';
import {ProfilePage} from '../pages/profile/profile';
import {VegPage} from '../pages/veg/veg';
import {NonvegPage} from '../pages/nonveg/nonveg';
import {CartPage} from '../pages/cart/cart';
import {BillsummaryPage} from '../pages/billsummary/billsummary';
import {PaymenttypePage} from '../pages/paymenttype/paymenttype';
import {PaymentPage} from '../pages/payment/payment';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DataProvider } from '../providers/data/data';
import { HttpClient } from '@angular/common/http';

import { SuperTabsModule} from 'ionic2-super-tabs';
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    FirstPage,
    LoginPage,
    SignupPage,
    MenuPage,
    DishlistPage,
    ProfilePage,
    VegPage,
    NonvegPage,
    CartPage,
    BillsummaryPage,
    PaymenttypePage,
    PaymentPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    FormsModule,
    ReactiveFormsModule,
    SuperTabsModule.forRoot(),
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    FirstPage,
    LoginPage,
    SignupPage,
    MenuPage,
    DishlistPage,
    ProfilePage,
    VegPage,
    NonvegPage,
    CartPage,
    BillsummaryPage,
    PaymenttypePage,
    PaymentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DataProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
