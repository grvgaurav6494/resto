import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the BillsummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-billsummary',
  templateUrl: 'billsummary.html',
})
export class BillsummaryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BillsummaryPage');
  }

  getAddress(){
    this.listAddress();
  }

  listAddress(){
    let alert = this.alertCtrl.create({
      title: 'Enter Address',
      cssClass:'radioname',
      inputs: [
        {
          type:'radio',
          label:'Address1',
          value:'address1 value'
        },
        {
          type:'radio',
          label: 'Address2',
          value:'address value2'
        },
        {
          type:'radio',
          label:'Address3',
          value:'address3 value'
        },
        {
          type:'radio',
          label:'Address4',
          value:'address 4 value',
        },
        {
          type:'radio',
          label:'Address5',
          value:'address value 5'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add Address',
          handler: data => {
            this.newAddress();
          }
        },
        {
          text: 'Confirm Order',
          handler: data => {
           
          }
        }
      ]
    });
    alert.present();
  }

  newAddress(){
    let alert = this.alertCtrl.create({
      title: 'Enter Address',
      inputs: [
        {
          name:'Pincode',
          placeholder:'Enter Pincode'
        },
        {
          name: 'First Line',
          placeholder: 'First Line Address'
        },
        {
          name:'Second line',
          placeholder:'Second line Address'
        },
        {
          name:'City',
          placeholder:'Enter City'
        },
        {
          name:'State',
          placeholder:'Enter State'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add Address',
          handler: data => {
           
          }
        }
      ]
    });
    alert.present();
  }

}
