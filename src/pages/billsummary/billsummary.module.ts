import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillsummaryPage } from './billsummary';

@NgModule({
  declarations: [
    BillsummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(BillsummaryPage),
  ],
})
export class BillsummaryPageModule {}
