import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import {DataProvider} from '../../providers/data/data';
import { SuperTabs } from 'ionic2-super-tabs';
import {VegPage} from '../veg/veg';
import { NonvegPage } from '../nonveg/nonveg';
import {CartPage} from '../cart/cart';
import {SignupPage} from '../signup/signup';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';

declare var $:any;
/**
 * Generated class for the DishlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dishlist',
  templateUrl: 'dishlist.html',
})
export class DishlistPage {

  message:any;
  page1: any=VegPage;
  page2:any=NonvegPage;
  page3:any=CartPage;
  itemcount=0;
  chkbtn=false;

  @ViewChild(SuperTabs) superTabs:SuperTabs;
  constructor(public events:Events,public storage:Storage,public navCtrl: NavController, public navParams: NavParams,public data:DataProvider) {
    this.events.subscribe('showCheckout',(val)=>{
      
      this.chkbtn=val;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DishlistPage');
  }

  ionViewWillEnter(){
    
    this.data.currentMessage.subscribe(message=>{
      this.message=message;
    })
  }

  slideToIndex(index){
    this.superTabs.slideTo(index);
  }

  hideToolbar(value:boolean){
    this.superTabs.showToolbar(!value);
  }

  item(value){
    if(value=='+'){
      this.itemcount++
    }else{
      if(this.itemcount!=0){
        this.itemcount--;
      }
    }
  }

  onTabSelect(val){
    // this.data.sendtype(val.index); 
  }

  currentDish($event){
    
  }

  getItems(val){

  }

  checkout(){
    
    this.storage.get('userDetails').then(user=>{
      if(user==undefined){
        this.navCtrl.setRoot(SignupPage);
      }
    })
  }

}
