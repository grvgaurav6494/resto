import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DishlistPage } from './dishlist';

@NgModule({
  declarations: [
    DishlistPage,
  ],
  imports: [
    IonicPageModule.forChild(DishlistPage),
  ],
})
export class DishlistPageModule {}
