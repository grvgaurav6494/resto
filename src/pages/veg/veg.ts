import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import {DataProvider} from '../../providers/data/data';
import { SuperTabs } from 'ionic2-super-tabs';

declare var $:any;
/**
 * Generated class for the VegPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-veg',
  templateUrl: 'veg.html',
})
export class VegPage {

  count: number;
  selected: any=[];
  itemcount=0;
  header:boolean=false;
  value:any;
  dishlist=[];
  typecount=0;
  type=0;
  dish=[];
  @ViewChild(SuperTabs) superTabs:SuperTabs;
  constructor(public events:Events,public navCtrl: NavController, public navParams: NavParams,public data:DataProvider) {
  }

  ionViewWillEnter(){
    
    this.value=this.navParams.get('value');
    // var selectdish=this.navParams.get('dish');
    if(this.value=='cart'){
      this.header=true;
    }

    this.dish=[{
      Soups:[
        {
          'type':'veg',
          'subtype':'chinese',
          'id':'VM1',
          'name':'Veg Manchow Soup',
          'status':'active',
          'price':'160',
          'description':'Chinese soup india mai chalta hai.',
          'count':0
        },
        {
          'type':'veg',
          'subtype':'chinese',
          'id':'VT1',
          'name':'Veg tomato Soup',
          'status':'active',
          'price':'150',
          'description':'Tomato soup india mai chalta hai.',
          'count':0
        },
        {
          'type':'nonveg',
          'subtype':'indian',
          'id':'NVC1',
          'name':'Chicken Mushroom Soup',
          'status':'active',
          'price':'200',
          'description':'Chinese chicken soup india mai zyada chalta hai.',
          'count':0
        },
        {
          'type':'nonveg',
          'subtype':'chinese',
          'id':'NVCM1',
          'name':'Nonveg chowmein Soup',
          'status':'active',
          'price':'220',
          'description':'Chinese CHOWMEIN soup india mai chalta hai.',
          'count':0
        }
      ],
      Biryani:[
        {
          'type':'veg',
          'subtype':'indian',
          'id':'VB1',
          'name':'Veg Biryani',
          'status':'active',
          'price':'180',
          'description':'Biryani india mai chalta hai.',
          'count':0
        },
        {
          'type':'nonveg',
          'subtype':'indian',
          'id':'NVCB1',
          'name':'Chicken Biryani',
          'status':'active',
          'price':'200',
          'description':'Chicken biryani india mai zyada chalta hai.',
          'count':0
        }
      ],
      Starters:[
        {
          'type':'veg',
          'subtype':'chinese',
          'id':'SVC1',
          'name':'Veg Crispy',
          'status':'active',
          'price':'140',
          'description':'Chinese veg crispy india mai chalta hai.',
          'count':0
        },
        {
          'type':'nonveg',
          'subtype':'chinese',
          'id':'NVCC1',
          'name':'Chicken crispy',
          'status':'active',
          'price':'200',
          'description':'Chinese chicken crispy india mai zyada chalta hai.',
          'count':0
        }
      ],
      Curries:[
        {
          'type':'veg',
          'subtype':'indian',
          'id':'VAM1',
          'name':'Allu mutter',
          'status':'active',
          'price':'160',
          'description':'Allu mutter india mai chalta hai.',
          'count':0
        },
        {
          'type':'nonveg',
          'subtype':'indian',
          'id':'NVCM1',
          'name':'Chicken Masala fry',
          'status':'active',
          'price':'200',
          'description':'Chinese masala fry india mai zyada chalta hai.',
          'count':0
        }
      ],
      Kebabs:[
        {
          'type':'veg',
          'subtype':'indian',
          'id':'VAM1',
          'name':'Mix veg Kebabs',
          'status':'active',
          'price':'160',
          'description':'Mix veg kebab india mai chalta hai.',
          'count':0
        },
        {
          'type':'nonveg',
          'subtype':'indian',
          'id':'NVCM1',
          'name':'Chicken Masala fry',
          'status':'active',
          'price':'200',
          'description':'Chinese masala fry india mai zyada chalta hai.',
          'count':0
        }
      ],
      Chinese:[
        {
          'type':'veg',
          'subtype':'chinese',
          'id':'VAM1',
          'name':'Veg Fried Rice',
          'status':'active',
          'price':'160',
          'description':'Veg fried rice india mai chalta hai.',
          'count':0
        },
        {
          'type':'nonveg',
          'subtype':'indian',
          'id':'NVCM1',
          'name':'Chicken Fried Rice',
          'status':'active',
          'price':'200',
          'description':'Chicken fried rice india mai zyada chalta hai.',
          'count':0
        }
      ]
    }]
    
    this.data.vegMessage.subscribe(prevveg=>{//prev dish 
    this.data.currentMessage.subscribe(selectdish=>{//which cuisine
      
      var plength=prevveg.length;
      if(plength!=0){
        this.dish=prevveg;
        var cuisine=prevveg[0][selectdish];
        for(var x=0;x<cuisine.length;x++){
          for(var y=0;y<plength;y++){
          if(cuisine[x].name==prevveg[y].name){
            cuisine[x]=prevveg[y];//now cuisine contains previous values
          } 
        }
      }
      }else{
        var cuisine=this.dish[0][selectdish];//first time visiting the veg page
      }
      
      this.dishlist=[];//emptying previous dishlist when code executes from quantity event
      for(var x=0;x<cuisine.length;x++){
        if(cuisine[x].type=='veg'){
          this.dishlist.push(cuisine[x]);
        }
      }
    })
    })
  }

  ionViewDidLoad() {
    var butwidth=$('.quantbut').width();
    $('ion-input').width(butwidth+32);
    console.log('ionViewDidLoad SubdishlistPage');
  }

  quantity(operation,pos,dish){
    if(operation=='+'){
      var replicaid=false;
      this.dishlist[pos].count++;
      
      this.events.publish('showCheckout',true);
    }else{
      if(this.dishlist[pos].count!=0){
        this.dishlist[pos].count--;
        if(this.dishlist[pos].count==0){
          let count=0;
        for(var x=0;x<this.dishlist.length;x++){
          if(this.dishlist[x].count==0){
            count++;
          }
        }
        if(this.dishlist.length==count){
          this.events.publish('showCheckout',false);
        }
        }
      }
    }
      
      this.data.vegDish(this.dish);//sending updated veg dishes
  }
  ionViewWillLeave(){
   
  }

}