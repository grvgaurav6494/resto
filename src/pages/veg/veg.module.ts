import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VegPage } from './veg';

@NgModule({
  declarations: [
    VegPage,
  ],
  imports: [
    IonicPageModule.forChild(VegPage),
  ],
})
export class VegPageModule {}
