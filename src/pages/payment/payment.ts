import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  header: any;
  upi: any = [];
  paylater: any = [];
  card: any = [];
  netbank: any = [];
  cash: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.upi = [{'type': 'PhonePe/Bhim UPI'}]

    this.paylater = [{'type': 'LazyPay'},{'type': 'ePayLater'}]

    this.card = [{'type': 'Debit Card'},{'type': 'Credit Card'}]

    this.netbank = [{ 'type': 'State Bank of India(SBI)' }, {'type': 'HDFC Bank'},{'type': 'ICICI Bank'},{'type': 'Canara Bank'},{'type': 'Axis Bank'},{'type':'Other Bank'}]

    this.cash = [{ 'type': 'Cash on delivery' }, { 'type': 'Pay Online on delivery' }]
  }

  ionViewWillEnter(){
    debugger
    this.header=this.navParams.get('value');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

  cardSelected(value){
    if(value="Debit Card"){
      
    }
  }
}
