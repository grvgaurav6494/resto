import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import {DataProvider} from '../../providers/data/data';
import { SuperTabs } from 'ionic2-super-tabs';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';

declare var $:any;
/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  selected: any=[];
  dishpresent: boolean=true;
  itemcount=0;
  header:boolean=false;
  value:any;
  dishlist:any=[];
  typecount=0;
  type=0;
  totaldish=[];
  @ViewChild(SuperTabs) superTabs:SuperTabs;
  constructor(public events:Events,public alertCtrl: AlertController,public storage: Storage,public navCtrl: NavController, public navParams: NavParams,public data:DataProvider) {
  }

  ionViewWillEnter(){
    
    if(this.navParams.get('value')=='cart'){
      this.header=true;
      this.value='Cart';
    }
    this.dishpresent=true;
    this.data.vegMessage.subscribe(vegdish=>{//new totaldish
        this.data.currentMessage.subscribe(cuisine=>{
          
        if(vegdish!=undefined && vegdish.length!=0){
          
          this.totaldish=vegdish; 
          var tot=this.totaldish[0];
          var dishname=Object.keys(tot);
          this.dishlist=[]//for again coming back to cart or adding quantity
          for(var w=0;w<dishname.length;w++){//for loop for every dish
            let cuisinedishes=tot[dishname[w]];//per dish length
              for(var x=0;x<cuisinedishes.length;x++){//for loop for adding items which are selected
                if(cuisinedishes[x].count>0){
                  this.dishlist.push(cuisinedishes[x]);
                }
          }
        }
        }else{
          
          this.dishpresent=false;
      }
      })
    })
  }

  ionViewDidLoad() {
    console.log(this.dishlist)
    var butwidth=$('.quantbut').width();
    $('ion-input').width(butwidth+32);
    console.log('ionViewDidLoad SubdishlistPage');
  }

  quantity(operation,pos,dish){
    if(operation=='+'){
      
      this.dishlist[pos].count++;
      this.events.publish('showCheckout',true);
    }else{
      
      if(this.dishlist[pos].count!=0){
        this.dishlist[pos].count--;
        if(this.dishlist[pos].count==0){
          this.showConfirm(dish,pos);
        }
      }
    }
  }


  ionViewWillLeave(){
      
  }

  showConfirm(dish,pos) {
    let confirm = this.alertCtrl.create({
      title: 'Remove Item',
      message: 'Are you sure you want to remove yummy '+dish.name+' ?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            this.dishlist[pos].count++;
          }
        },
        {
          text: 'Agree',
          handler: () => {
            this.dishlist.pop(dish);
            if(this.dishlist.length==0){
              this.dishpresent=false; 
            }
            this.events.publish('showCheckout',false);
            this.data.vegDish(this.totaldish)
          }
        }
      ]
    });
    confirm.present();
  }
}
