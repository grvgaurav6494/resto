import { Component,ViewChild,Renderer,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';

/**
 * Generated class for the PaymenttypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-paymenttype',
  templateUrl: 'paymenttype.html',
})
export class PaymenttypePage implements OnInit{

  upiState=false;
  cardState=false;
  netbankState=false;
  codState=false;
  paylaterState=false;

  @ViewChild('upitype') upiList:any
  @ViewChild('cardtype') cardList:any;
  @ViewChild('netbanktype') netbankList:any;
  @ViewChild('codtype') codList:any;
  @ViewChild('paylatertype') paylaterList:any;

  upi: any = [];
  paylater: any = [];
  card: any = [];
  netbank: any = [];
  cash: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,public renderer:Renderer) {
    this.upi = [{'type': 'PhonePe/Bhim UPI'}]

    this.paylater = [{'type': 'LazyPay'},{'type': 'ePayLater'}]

    this.card = [{'type': 'Debit Card'},{'type': 'Credit Card'}]

    this.netbank = [{ 'type': 'State Bank of India(SBI)' }, {'type': 'HDFC Bank'},{'type': 'ICICI Bank'},{'type': 'Canara Bank'},{'type': 'Axis Bank'},{'type':'Other Bank'}]

    this.cash = [{ 'type': 'Cash on delivery' }, { 'type': 'Pay Online on delivery' }]
  }

  ngOnInit(){
    this.renderer.setElementStyle(this.upiList.nativeElement,'webkitTransition','max-height 700ms');
    this.renderer.setElementStyle(this.codList.nativeElement,'webkitTransition','max-height 700ms');
    this.renderer.setElementStyle(this.paylaterList.nativeElement,'webkitTransition','max-height 700ms');
    this.renderer.setElementStyle(this.netbankList.nativeElement,'webkitTransition','max-height 700ms');
    this.renderer.setElementStyle(this.cardList.nativeElement,'webkitTransition','max-height 700ms');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

  cardSelected(value){
    if(value="Debit Card"){
      
    }
  }

  paymentSelect(type){
    debugger
    var val={
      'value':type
    }
    this.navCtrl.push(PaymentPage,val)
  }

  toggleAcc(val){
    debugger
    switch(val){
      case 'upi':
        if(this.upiState){
          this.renderer.setElementStyle(this.upiList.nativeElement,'max-height','0px');
        }else{
          this.renderer.setElementStyle(this.upiList.nativeElement,'max-height','500px');
          this.renderer.setElementStyle(this.cardList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.netbankList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.codList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.paylaterList.nativeElement,'max-height','0px');
        }
        this.upiState=!this.upiState
        break;
    case 'card':
        if(this.cardState){
          this.renderer.setElementStyle(this.cardList.nativeElement,'max-height','0px');
        }else{
          this.renderer.setElementStyle(this.cardList.nativeElement,'max-height','500px');
          this.renderer.setElementStyle(this.upiList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.netbankList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.codList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.paylaterList.nativeElement,'max-height','0px');
        }
        this.cardState=!this.cardState
        break;
    case 'netbank':
        if(this.netbankState){
          this.renderer.setElementStyle(this.netbankList.nativeElement,'max-height','0px');
        }else{
          this.renderer.setElementStyle(this.netbankList.nativeElement,'max-height','500px');
          this.renderer.setElementStyle(this.cardList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.upiList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.codList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.paylaterList.nativeElement,'max-height','0px');
        }
        this.netbankState=!this.netbankState
        break;
    case 'cod':
        if(this.codState){
          this.renderer.setElementStyle(this.codList.nativeElement,'max-height','0px');
        }else{
          this.renderer.setElementStyle(this.codList.nativeElement,'max-height','500px');
          this.renderer.setElementStyle(this.cardList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.netbankList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.upiList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.paylaterList.nativeElement,'max-height','0px');
        }
        this.codState=!this.codState
        break;
    case 'paylater':
        if(this.paylaterState){
          this.renderer.setElementStyle(this.paylaterList.nativeElement,'max-height','0px');
        }else{
          this.renderer.setElementStyle(this.paylaterList.nativeElement,'max-height','500px');
          this.renderer.setElementStyle(this.cardList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.netbankList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.codList.nativeElement,'max-height','0px');
          this.renderer.setElementStyle(this.upiList.nativeElement,'max-height','0px');
        }
        this.paylaterState=!this.paylaterState
        break;
  }
  }
}

