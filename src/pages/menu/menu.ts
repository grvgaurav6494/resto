import { Component, ViewChild, AfterViewInit,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Slides} from 'ionic-angular';
import { DishlistPage } from '../dishlist/dishlist';
import {DataProvider} from '../../providers/data/data';
import { ProfilePage } from '../profile/profile';
import { CartPage } from '../cart/cart';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage implements AfterViewInit,OnInit {
  @ViewChild(Slides) slides:Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams,public data:DataProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  ngOnInit(){

  }


  ngAfterViewInit(){
    this.slides.autoplay=1500;
    this.slides.loop=true;
  }

  gotoSlide(){
    this.slides.startAutoplay();
  }

  slideChanged(){
    // let currentIndex=this.slides.slideNext(500);
    this.slides.startAutoplay()
  }

  godish(value){
    this.navCtrl.push(DishlistPage);
    this.data.sendmsg(value);
  }

  goCart(){
    var val={
      'value':'cart'
    }
    this.navCtrl.push(CartPage,val);
  }

  goProfile(){
    this.navCtrl.push(ProfilePage);
  }

}
