import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormGroup,FormControl,FormBuilder,Validators} from '@angular/forms'
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  
  signupForm: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,public fb:FormBuilder) {
    var pattern='/^(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/|^[789]\d{9}$)$/';
    var mobile="^[789]\d{9}$)$/"
    this.signupForm=this.fb.group({
      mobile:Validators.compose([Validators.required,Validators.pattern(pattern),Validators.minLength(10),Validators.maxLength(10)]),
      name:'',
      password:'',
      repassword:''
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  signup(){
    debugger
  }

}
