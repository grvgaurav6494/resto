import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NonvegPage } from './nonveg';

@NgModule({
  declarations: [
    NonvegPage,
  ],
  imports: [
    IonicPageModule.forChild(NonvegPage),
  ],
})
export class NonvegPageModule {}
